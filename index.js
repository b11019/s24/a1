// alert("ACTIVITY 24 NA!")

// S24 Activity:

/*>> In the S19 folder, create an activity folder and an index.html and index.js file inside of it.

>> Link the script.js file to the index.html file.*/

/*>> Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

>> Using Template Literals, print out the value of the getCube variable with a message:
		 The cube of <num> is…*/

		 // S O L U T I O N

	const getCube = 190 ** 3
	console.log(`The cube of 190 is ${getCube}`);

/*>> Destructure the given array and print out a message with the full address using Template Literals.*/
	
		const address = { 
						address1: "258", 
						address2: "Washington Ave NW", 
						address3: "California", 
						address4: "90011"};

		// message: I live at <details>
		const {address1 , address2, address3, address4} = address
		let message = `I live at ${address1}, ${address2}, ${address3} ${address4}.`
		console.log(message)






// >> Destructure the given object and print out a message with the details of the animal using Template Literals.
	
		const animal = {
			name: "Lolong",
			species: "saltwater crocodile",
			weight: "1075 kgs",
			measurement: "20 ft 3 in"
		};

		// message: <name> was a <species>. He weighed at <weight> with a measurement of <measurement>.

		const {name, species, weight , measurement} = animal
		let msg = `${name} was a ${species}. He weighed at ${weight} with a ${measurement}.`
		console.log(msg);




// >> Loop through the given array of characters using forEach, an arrow function and using the implicit return statement to print out each character
		
		const characters = ['Ironman', 'Black Panther', 'Dr. Strange', 'Shang-Chi', 'Falcon']

		characters.forEach(character => console.log(`${character}`))

// >> Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

		class MyDog {
			constructor(name, age, breed){
				this.name = name;
				this.age = age;
				this.breed = breed
			}
		}
		let myOldDog = new MyDog (`Chuchi`, 3, `Hound Dog x Retriever`)
		console.log(myOldDog);

// >> Create/instantiate a 2 new object from the class Dog and console log the object

let myOtherDog = new MyDog (`Sam`, 2, `Aspin`)
console.log(myOtherDog);